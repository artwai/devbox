#!/usr/bin/env bash

# WIP script for "real" Ubuntu installation
./upgrade-linux.sh
./generate-sshkey.sh
./install-git.sh
./install-apache.sh
./install-php.sh
./install-mysql.sh
./install-wp-cli.sh
./install-nodejs.sh
./install-python2.sh
./install-composer.sh
./install-unzip.sh
./install-jq.sh
./install-mailhog.sh
./install-mkcert.sh # TODO add certificate to chrome/firefox