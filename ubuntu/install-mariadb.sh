# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-mariadb.sh

# Port TCP sur lequel mariadb sera disponible
SQL_PORT=13306

# Installe Mariadb
echo "Installation de MariaDb"
apt-get -qq install mariadb-server > /dev/null

# Configuration initiale de Mariadb
echo "Configuration du port TCP ($SQL_PORT)"
file='/etc/mysql/conf.d/mysql.cnf'
printf "\n[mysqld]\nport=13306\n" >> $file

# Démarrage
echo "Démarrage du service mysql"
service mysql start > /dev/null

# Exécute le script mysql_secure_installation
echo "Sécurisation de l'installation"

#
#       Enter current password for root (enter for none)
#       | Set root password? [Y/n]
#       | |  Remove anonymous users? [Y/n]
#       | |  |  Disallow root login remotely? [Y/n]
#       | |  |  |  Remove test database and access to it? [Y/n]
#       | |  |  |  |  Reload privilege tables now? [Y/n]
#       | |  |  |  |  |
#       v v  v  v  v  v
printf "\nN\nY\nN\nY\nY\n" | script -q -c 'mysql_secure_installation' > /dev/null

# remarque : mysql_secure_installation est un script interactif, il s'attend à lire les réponses
# depuis un terminal et ça génère un warning si on les lui fournit via une redirection.
# pour éviter ça, on utilise la commande script qui génère un pseudo terminal pty à partir de
# l'entrée standard (paramètres : -q pour quiet, -c commande à exécuter)
