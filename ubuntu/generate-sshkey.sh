# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# generate-sshkey.sh


# Ce que fait ce script :
# -----------------------
# Il installe une clé SSH dans WSL basée sur ed25519 pour des raisons de performance et sécurités.
# L'emplacement est celui par défaut dans le répertoire du user courant (non root).
# Le mot de passe est laissé vide.

# Passage sur l'utilisateur par défaut : dev
if uname -a | grep -q '^Linux.*Microsoft'; then
	su dev
fi

# Génération de la clé
if [ ! -f ~/.ssh/id_ed25519 ]; then
	ssh-keygen -t ed25519 -q -f ~/.ssh/id_ed25519 -N ""
fi

# Affichage
echo "Voici ta clé ssh publique, note-la, elle te sera utile pour GitHub :"
cat ~/.ssh/id_ed25519.pub

# retour en root
sudo su