# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-mailhog.sh
# - installe golang
# - installe le service d'attrapeur de mail MailHog

# Path de base des scripts devbox sur le dépôt GitLab
# todo: pour éviter de répéter, modifier install-devbox.ps1 pour qu'il nous fournisse le path en paramètre
host='https://gitlab.com/artwai/devbox/-/raw/master/ubuntu/'

# Installe le fichier /usr/lib/systemd/system/mailhog.service
file='usr/lib/systemd/system/mailhog.service'
echo "> Création du fichier /$file"
sudo wget -q -O /$file $host$file || echo "Erreur wget, impossible de télécharger le fichier"

# Installe Golang
echo "Installation de Golang (cela peut prendre plusieurs minutes)"
apt-get -y install golang-go > /dev/null
go version
echo

# Installe MailHog
echo "Installation de MailHog"
su dev
go install github.com/mailhog/MailHog@latest > /dev/null

sudo ln -s /home/dev/go/bin/MailHog /usr/local/bin/mailhog
echo

sudo su
systemctl daemon-reload
systemctl start mailhog
systemctl enable mailhog
