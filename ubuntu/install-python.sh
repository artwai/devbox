# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-python.sh

# Installe Git
echo "Installation de Python 3"
apt-get -qq install python3 > /dev/null

# Affichage des info de Python 2
python3 --version
