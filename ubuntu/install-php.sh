# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-php.sh
# - Ajoute le ppa ondrej/php pour pouvoir installer plusieurs versions de php en même temps
# - installe php 7.4 et php 8.0

# Versions de PHP qu'on veut installer (array)
PHP_VERSIONS=("7.4" "8.0" "8.1" "8.2")

# Version par défaut qui sera activée si le vhost ne définit pas sa propre directive SetHandler
PHP_DEFAULT_VERSION="8.0"

# Extensions php à installer pour chaque version (le préfixe "phpx.y-" est ajouté automatiquement)
PHP_EXTENSIONS=("curl" "gd" "gmp" "intl" "mbstring" "mysql" "soap" "sqlite3" "xdebug" "xml" "zip" )
# Package php-dom is a virtual package provided by php-xml

# fpm : installée séparément

## Peut-être à ajouter
# imap
# ldap
# xml-rpc


## Modules activés par défaut :
# calendar
# Core
# ctype
# date
# exif
# FFI
# fileinfo
# filter
# ftp
# gettext
# hash
# iconv
# json
# libxml
# openssl
# pcntl
# pcre
# PDO
# Phar
# posix
# readline
# Reflection
# session
# shmop
# sockets
# sodium
# SPL
# standard
# sysvmsg
# sysvsem
# sysvshm
# tokenizer
# zlib

# Ajoute le ppa ondrej/php
echo "Ajout du dépôt PPA de Ondřej Surý..."

echo "- apt-get install software-properties-common"
apt-get -qq install software-properties-common > /dev/null

echo "- add-apt-repository ppa:ondrej/php"
LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php > /dev/null

echo "- apt-get update"
apt-get -qq -y update > /dev/null

# Installe les versions de PHP et les extensions demandées
for version in ${PHP_VERSIONS[@]}; do
    # extensions=$(echo $PHP_EXTENSIONS | sed "s/[^ ]* */php$version-&/g")

    # php
    echo "Installation de PHP $version :"
    echo "- php$version"
    apt-get -qq install php$version > /dev/null

    # fpm
    echo "- php$version-fpm"
    apt-get -qq install php$version-fpm > /dev/null

    # autres extensions
    for extension in ${PHP_EXTENSIONS[@]}; do
        echo "- php$version-$extension"
        apt-get -qq install php$version-$extension > /dev/null
    done

    # php.ini
    echo "- paramétrage de php.ini"
    file="/etc/php/$version/fpm/php.ini"
    echo "  > post_max_size = 1024M"
    sed -i 's/post_max_size = .*/post_max_size = 1024M/' $file
    echo "  > memory_limit = 1024M"
    sed -i 's/memory_limit = .*/memory_limit = 1024M/' $file

    # pool fpm
    echo "- paramétrage du pool par défaut"
    file="/etc/php/$version/fpm/pool.d/www.conf"

    # Par défaut fpm écoute sur le port TCP 9000. Pour éviter des conflits éventuels (fpm déjà
    # installé sur l'hôte windows), on fait +10000 (comme pour les ports apache) et on utilise les
    # deux derniers chiffres pour indiquer la version de php (7.4 -> 19074, 8.0 -> 19080, etc.)
    # A priori, ces ports sont dispos, il ne sont pas réservés dans iana :
    # https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=19074
    port=$(echo 190$version | sed "s/\.//")
    echo "  > listen = 127.0.0.1:$port"
    sed -i "s/listen = .*/listen = 127.0.0.1:$port/" $file

    echo "  > pm = ondemand"
    sed -i "s/pm = .*/pm = ondemand/" $file

    echo "  > user & group sur dev"
    sed -i "s/user = www-data/user = dev/" $file
    sed -i "s/group = www-data/group = dev/" $file
    sed -i "s/listen.owner = www-data/listen.owner = dev/" $file
    sed -i "s/listen.group = www-data/listen.group = dev/" $file

    echo "  > pm.max_children = 20"
    sed -i "s/pm.max_children = .*/pm.max_children = 20/" $file

    # Configuration apache : désactive php-module et fait le paramètrage php-fpm
    echo "- paramétrage apache"
    a2dismod php$version > /dev/null

    host='https://gitlab.com/artwai/devbox/-/raw/master/ubuntu/' # todo: not DRY
    src="${host}etc/apache2/conf-available/php-fpm.conf"
    dst="/etc/apache2/conf-available/php$version-fpm.conf"  # on écrase le fichier par défaut qui existe
    wget -q -O $dst $src || echo "Erreur wget, impossible de télécharger le fichier"
    sed -i "s/PHP_FPM_PORT/$port/" $dst

    # Affichage de la version installée
    php$version -v
done

# Active la version par défaut de PHP dans apache
a2enconf php${PHP_DEFAULT_VERSION}-fpm > /dev/null

# Ative la version par défaut pour PHP-CLI
sudo ln -sfn /usr/bin/php${PHP_DEFAULT_VERSION} /etc/alternatives/php