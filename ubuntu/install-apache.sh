# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-apache.sh
# - installe apache
# - définit des variables d'environnement qui indiquent les ports sur lesquels apache écoute
# - reconfigure apache pour qu'il écoute sur les ports définis

# Pour éviter tout conflit avec ce qui peut être installé sur l'hôte, on affecte des ports TCP
# particuliers à apache sous la forme 10000 + port normallement utilisé.

# Information sur les ports
# https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml
# Attention cette liste n'est à jour qu'en fonction que ce que les entreprises veulent bien déclarer (Microsoft ne le fait pas forcément)

# - http : 33080
APACHE_HTTP_PORT=80

# - https : 443
APACHE_HTTPS_PORT=443

# Met à jour les paquets
# echo "Mise à jour des librairies..."
# apt-get -qq update

# Installe apache
# apt-get -qq remove apache2
echo "Installation de Apache"
apt-get -qq install apache2 > /dev/null
apache2ctl -v
echo

# Normalement, à ce stade, apache est démarré (s'il a pu).
# Sur WSL ce n'est pas le cas car on n'a pas systemd
# On fait tout de même un "stop" au cas où
service apache2 stop > /dev/null

# Définit les ports sur lesquels on veut que notre apache écoute
echo "Configuration des variables d'environnement"
file='/etc/apache2/envvars'
printf "\n## >devbox\n" >> $file
printf "export APACHE_HTTP_PORT=%s\n" $APACHE_HTTP_PORT >> $file
printf "export APACHE_HTTPS_PORT=%s\n" $APACHE_HTTPS_PORT >> $file
printf "## <devbox\n" >> $file

# J'aurais préféré ça, mais apparemment pas exécuté lors du démarrage d'apache
# file='/etc/profile.d/devbox.sh'
# printf "export APACHE_HTTP_PORT=%s\n" $APACHE_HTTP_PORT >> $file
# printf "export APACHE_HTTPS_PORT=%s\n" $APACHE_HTTPS_PORT >> $file
# chmod +x $file

# Modifie le fichier /etc/apache2/ports.conf pour que Apache écoute sur les ports indiqués
echo "Modification des ports TCP"
file='/etc/apache2/ports.conf'
sed -i 's/Listen 80/Listen ${APACHE_HTTP_PORT}/gI' $file
sed -i 's/Listen 443/Listen ${APACHE_HTTPS_PORT}/gI' $file

# Modifie le port dans les sites par défaut de Apache
echo "Modification des sites par défaut"
file='/etc/apache2/sites-available/000-default.conf'
sed -i 's/\(<VirtualHost.*\):80\(.*>\)/\1:${APACHE_HTTP_PORT}\2/gI' $file

file='/etc/apache2/sites-available/default-ssl.conf'
sed -i 's/\(<VirtualHost.*\):443\(.*>\)/\1:${APACHE_HTTPS_PORT}\2/gI' $file

# Active les modules apache
echo "Activation des modules apache dont on a besoin"
a2enmod headers rewrite ssl proxy_fcgi setenvif > /dev/null

# Active le site ssl par défaut de Apache
echo "Activation de SSL"
a2ensite default-ssl.conf > /dev/null
