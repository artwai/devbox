# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-composer.sh


# Ce que fait ce script :
# -----------------------
# Il installe Composer version 1 sous l'alias composer1 et la version 2 sous les aliases composer et composer2

## Le hash changeant régulièrement, il n'est pas possible d'utiliser le système proposé par Composer :
# # Récupération de l'installateur et vérification du téléchargement
# php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# php -r "if (hash_file('sha384', 'composer-setup.php') !== '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { unlink('composer-setup.php'); }"

# # Si le fichier est OK, on  continue
# if [ -f composer-setup.php ];then
# 	echo "Installation de Composer version 1.x., utilisable avec l'alias 'composer1'"
# 	php composer-setup.php --1  > /dev/null
# 	sudo mv composer.phar /usr/local/bin/composer1
# 	echo "Installation de Composer version 2.x, utilisable avec les alias 'composer' ou 'composer2'"
# 	php composer-setup.php --2  > /dev/null
# 	sudo mv composer.phar /usr/local/bin/composer2
# 	sudo ln -sf /usr/local/bin/composer2 /usr/local/bin/composer
# 	php -r "unlink('composer-setup.php');"
# else
# 	echo 'Installateur corrompu.'
# 	echo 'Installation échouée.'
# fi

echo "Téléchargement de la version 1 de Composer"
curl -s -O https://getcomposer.org/download/latest-1.x/composer.phar > /dev/null
sudo mv composer.phar /usr/local/bin/composer1

echo "Téléchargement de la version 2 de Composer"
curl -s -O https://getcomposer.org/download/latest-2.x/composer.phar > /dev/null
sudo mv composer.phar /usr/local/bin/composer2
sudo ln -sf /usr/local/bin/composer2 /usr/local/bin/composer

