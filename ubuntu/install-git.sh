# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-git.sh

# Avec l'image ubuntu-20.04 qu'on utilise pour installer la devbox, git est déjà pré-installé
# On fait tout de même un apt install (qui dit juste "vous avez déjà la dernière version") au
# cas où cela change à l'avenir.

# Installe Git
echo "Installation de Git"
apt-get -qq install git git-lfs > /dev/null
git --version

# Source le fichier .bashrc qui apparemment n'est pas lu quand le script est excuté depuis powershell
source /root/.bashrc

# Configuration de Git
echo "Paramétrage de Git"
#pour l'utilisateur courant, pas root
su dev
source "$HOME/.bashrc"

if uname -a | grep -q '^Linux.*Microsoft'; then
	# Désactive la conversion LF<->CRLF
	sudo git config --system core.autocrlf false

	# Indique à git le nom et le mail de l'utilisateur (on utilise les vars créées par create-user.sh)
	sudo git config --system user.name "$DEVBOX_USER_NAME"
	sudo git config --system user.email "$DEVBOX_USER_MAIL"
fi

echo -e "Host a4i-wp-plugins.github.com\n\
    HostName github.com\n\
    User git" >> ~/.ssh/config
