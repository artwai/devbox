# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-mysql.sh

# Port TCP sur lequel mysql sera disponible
SQL_PORT=13306

# Installation de MySQL
echo "Installation de MySQL"
apt-get -qq install mysql-server > /dev/null
mysql --version

# Configuration initiale de MySQL
echo "Configuration du port TCP ($SQL_PORT)"
file1='/etc/mysql/conf.d/mysql.cnf'
printf "\n[mysqld]\nport=13306\n" >> $file1

# Exécute le script mysql_secure_installation
# echo "Sécurisation de l'installation"
# "Connecting to MySQL using a blank password."
#         Would you like to setup VALIDATE PASSWORD component?
#         |  Please set the password for root here
#         |  |    Re-enter new password
#         |  |    |    Remove anonymous users? [Y/n]
#         |  |    |    |  Disallow root login remotely? [Y/n]
#         |  |    |    |  |  Remove test database and access to it? [Y/n]
#         |  |    |    |  |  |  Reload privilege tables now? [Y/n]
#         |  |    |    |  |  |  |
#         v  v    v    v  v  v  v
# printf "N\ndev\ndev\nY\nN\nY\nY\n" | script -q -c 'mysql_secure_installation' > /dev/null

# Configuration de la connexion automatique à MySQL
echo "Configuration de la connexion automatique à MySQL"
file2='/home/dev/.my.cnf'
printf "\n[mysql]\nuser=dev\npassword=dev" >> $file2

# remarque : mysql_secure_installation est un script interactif, il s'attend à lire les réponses
# depuis un terminal et ça génère un warning si on les lui fournit via une redirection.
# pour éviter ça, on utilise la commande script qui génère un pseudo terminal pty à partir de
# l'entrée standard (paramètres : -q pour quiet, -c commande à exécuter)
