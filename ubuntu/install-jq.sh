# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-jq.sh

# Installe jq
echo "Installation de jq"
apt-get -qq install jq > /dev/null

jq --version