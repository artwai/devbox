# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# upgrade-linux.sh

# Ce que fait ce script :
# -----------------------
# Il mets à jour Linux et nettoie l'inutile

echo "- apt-get update"
apt-get -qq -y update > /dev/null

echo "- apt-get dist-upgrade"
apt-get -qq -y dist-upgrade > /dev/null

if uname -a | grep -q '^Linux.*Microsoft'; then
	echo "- apt-get autoremove"
	apt-get -qq -y autoremove > /dev/null
fi