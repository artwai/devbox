# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# restart-wsl.sh


# Ce que fait ce script :
# -----------------------
# il execute une action basique permettant d'initier le lancement de la devbox
echo "Démarrage OK."