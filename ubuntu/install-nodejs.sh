# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-nodejs.sh

# Ce que fait ce script :
# -----------------------
# Il installe NodeJS permettant d'utiliser, entre autres, Gulp.
# Installe Gulp en global.
# Nécessite cURL (installé avec la distro) et PHP, installé via le script install-php.sh

# Passage sur l'utilisateur par défaut : dev
su dev
cd ~

# Installation du dépôt NodeSource correspondant à la dernière version LTS de Node.js en cours
echo "Téléchargement de la version LTS de NodeJS (version 22.x)"
curl -fsSL https://deb.nodesource.com/setup_22.x -o nodesource_setup.sh
sudo -E bash nodesource_setup.sh
rm nodesource_setup.sh

# Installation de NodeJS
echo "Installation de NodeJS"
sudo apt-get install nodejs > /dev/null

# Affichage des info
node --version

echo "Encore pas mal de projet en node 14, création d'un .nvmrc dans le dossier utilisateur"
echo "14" > /home/dev/.nvmrc

# retour en root
sudo su

# Installation de Yarn en global
echo "Installation de Yarn en global"
npm install --silent -g yarn > /dev/null
yarn --version

# Installation de Gulp en global
echo "Installation de Gulp en global"
npm install --silent -g gulp > /dev/null
gulp --version
