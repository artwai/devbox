# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-wp-cli.sh

# Ce que fait ce script :
# -----------------------
# Il installe WP Cli qui permet d'effectuer diverses actions sur WordPress en lignes de commande.
# Nécessite cURL (installé avec la distro) et PHP, installé via le script install-php.sh

# Passage sur l'utilisarteur par défaut : dev
if uname -a | grep -q '^Linux.*Microsoft'; then
	su dev
fi
cd ~

# Tééléchargment du fichier PHA
echo "Téléchargement de la dernière version de WP Cli"
curl -s -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar > /dev/null

# vérification des infos de wp-cli pour vérifier que le téléchargement est OK
# php wp-cli.phar --info
chmod +x wp-cli.phar

# Déplacement pour reconnaissance par l'environnement et utilisation globale.
sudo mv wp-cli.phar /usr/local/bin/wp

# Affichage des info de WP-Cli
wp --info

if uname -a | grep -q '^Linux.*Microsoft'; then
	# retour en root
	sudo su
fi