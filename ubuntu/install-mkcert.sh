# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-mkcert.sh

# Téléchargement de la bibliothèque nécessaire
apt-get -qq install libnss3-tools > /dev/null

# Récupération de la dernière version en cours
# latest_v_version est la version avec le "v" devant, comme "v1.4.3"
# latest_version est juste le numéro de version, par exemple "1.4.3"
latest_v_version="$(curl --silent "https://api.github.com/repos/FiloSottile/mkcert/releases/latest" | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')"
latest_version=${latest_v_version:1}


# Téléchargement de mkcert
wget -q https://github.com/FiloSottile/mkcert/releases/download/$latest_v_version/mkcert-$latest_v_version-linux-amd64 -O mkcert > /dev/null
chmod +x mkcert

mv mkcert /usr/local/bin/mkcert

# Affichage version installée
echo $latest_v_version