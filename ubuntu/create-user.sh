# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# create-user.sh
# - crée le compte utilisateur de la devbox (dev /dev)
# - l'ajoute comme sudoers
# - permet d'invoquer sudo sans avoir à saisir le mot de passe
# - indique à WSL l'utilisateur par défaut


# Demande le nom et le mail de l'utilisateur, on en a besoin par la suite (git, clés ssh...)
echo 'Faisons connaissance !'
echo "   Dans les lignes qui suivent, je vais te demander ton nom et ton adresse e-mail."
echo "   Saisis ton nom en veillant aux initiales et aux accents et veille à ne pas faire"
echo "   d'erreur en saisissant ton adresse e-mail !"
echo

name=""
while ! [[ "$name" =~ ^[A-Z].*[a-z].*$ ]]
do
  read -p "   - Tape ton nom (sous la forme \"Prénom Nom\") : " name < /dev/tty
done

mail=""
while ! [[ "$mail" =~ ^.*@.*$ ]]
do
  read -p "   - Tape ton adresse e-mail : " mail < /dev/tty
done

# Stocke le nom et le mail de l'utilisateur dans le fichier /etc/profile.d/devbox-vars.sh
file="/etc/profile.d/devbox-vars.sh"
printf "\n" >> $file
printf "export DEVBOX_USER_NAME='$name'\n" >> $file
printf "export DEVBOX_USER_MAIL='$mail'\n" >> $file

# Crée le compte de l'utilisateur, son groupe et son home directory
# source : https://askubuntu.com/a/1307156
username=dev
password=dev

echo "Création de ton compte utilisateur"
adduser --gecos "" --disabled-password $username > /dev/null
chpasswd <<<"$username:$password"

# Ajoute l'utilisateur au groupe sudo et évite le message "type sudo to run a command as root"
usermod -aG sudo $username
touch /home/$username/.sudo_as_admin_successful

# Sudo sans password
printf "\n$username ALL=(ALL) NOPASSWD:ALL\n" >> /etc/sudoers

# Crée le fichier wsl.conf (indique l'utilisateur par défaut à WSL)
# https://docs.microsoft.com/en-us/windows/wsl/wsl-config#configure-per-distro-launch-settings-with-wslconf
# Il faut redémarrer la distro pour que ça prenne effet (wsl --terminate devbox)
wslconf='/etc/wsl.conf'
printf "[user]\n" >> $wslconf
printf "default=$username\n" >> $wslconf

# Modifie le fichier .bashrc de l'utilisateur
file="/home/$username/.bashrc"

# Modifie le prompt pour qu'il inclue le nom de la devbox
sed -i 's/\(PS1=.*\)@\\h\(.*\)/\1@$WSL_DISTRO_NAME.\\h\2/' $file

# Ajoute un alias "h" pour "history"
printf "\n" >> $file
printf "alias h='history'\n" >> $file

# Va dans le home dir de l'utilisateur (WSL va toujours dans le répertoire windows en cours)
printf "cd ~\n" >> $file
