# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# create-dbuser.sh


# Ce que fait ce script :
# -----------------------
# Après redémarrage de la devbox, il se connecte en su à MySQL.
# Il crée un utilisateur "dev" avec le mot de passe "dev" et lui donne tous les droits.
# sudo service mysql start > /dev/null
sudo mysql -uroot mysql -e "CREATE USER 'dev'@'localhost' IDENTIFIED BY 'dev'; CREATE USER 'dev'@'%' IDENTIFIED BY 'dev'; GRANT ALL ON *.* TO 'dev'@'localhost'; GRANT ALL ON *.* TO 'dev'@'%'; FLUSH PRIVILEGES;"