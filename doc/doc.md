# Install-devbox

`Install-devbox` est un ensemble de scripts qui permettent de créer un environnement de développement
Ubuntu sur un PC Windows (une "devbox").

Il crée une distribution Ubuntu basée sur WSL et installe les outils usuels dont on a besoin pour
développer des sites web (git, apache, php, mysql, nodejs, etc.)

![welcome](images/welcome.jpg "Script d'installation")

## Prérequis

- Un PC sous Windows 10 ou plus (build >= 19043, vous pouvez vérifier avec `win+R` + `winver`),
- Au moins 8 Go de RAM,
- Au moins 3 Go d'espace disque disponible sur le disque C.

## Installation

1. Téléchargez [le script d'installation](https://gitlab.com/artwai/devbox/-/raw/master/windows/install-devbox.bat?inline=false)
   depuis le dépôt GitLab du  projet et enregistrez le dans un répertoire temporaire.
2. Lancez le script en double-cliquant dessus. Votre anti-virus va immédiatement vous afficher
   une alerte indiquant qu'il a bloqué l'exécution d'un script téléchargé depuis internet :
   **indiquez à votre antivirus que vous voulez quand même lancer le script.**

   - Exemple avec Microsoft Defender (l'antivirus standard de Windows) :

     ![defender1](images/defender1.jpg "Alerte affichée par Microsoft Defender")

   - Cliquez sur *Informations complémentaires*, l'option *Exécuter quand même* apparaît :

     ![defender2](images/defender2.jpg "Informations complémentaires")

   - Cliquez sur ***Exécuter quand même***.

3. L'UAC de Windows affiche un warning indiquant que le script va apporter des modifications à
   votre PC, cliquez "oui" pour autoriser l'exécution du script :

   ![uac](images/uac.png "Contrôle de compte utilisateur")


4. Suivez ensuite les instructions qui s'affichent.

  L'installation prend 5 à 10 minutes selon votre connexion internet et votre vitesse de lecture.

## Qu'est-ce que cela installe ?

`Install-devbox` fait très peu de modifications sur votre poste :
- Côté windows, il active (si ce n'est pas déjà fait) le module WSL qui est un composant standard
  de Windows.
- Tous les fichiers créés par le script d'installation sont regroupés dans le répertoire `c:\devbox`
  de votre PC.
- Aucun autre logiciel Windows n'est installé (on télécharge juste l'utilitaire mkcert.exe mais
  il n'installe rien).

Tout le reste est installé au sein de la distribution WSL qui est installée :
- Git
- Apache
- PHP 7.4 et 8.0
- MySQL 8
- WP-Cli
- NodeJS
- Python 2

Les paramètres de la devbox ont été choisis avec soin pour limiter autant que possible tout
risque de conflit éventuel avec des choses déjà installées sur le poste : numéros de port apache
et mysql spécifiques, système de fichiers distinct, instance à part de mysql, etc.

En cas de besoin, la devbox peut facilement être désinstallée et le poste se retrouve dans
l'état où il était avant le lancement du script (todo: faire doc de désinstallation). Il suffit
pour cela de
- désinscrire la distro WSL (`wsl --unregister devbox`),
- supprimer le répertoire `c:\devbox` (vérifier qu'il n'y a pas d'autres distros dedans),
- supprimer les variables d'environnement `CAROOT` et `WSLENV` (setx ?).

## FAQ (work in progress)


### La devbox est toujours lancée ?

> La devbox n'est pas lancée au démarrage de votre PC : si vous n'utilisez pas votre devbox, elle
> ne consomme ni RAM ni CPU sur votre PC et ne génère aucun ralentissement.
>
> La devbox est démarrée lorsque vous vous y connectez pour la première fois, soit manuellement,
> soit via des outils qui s'y connecte automatiquement comme Visual Studio Code ou Windows Terminal.

### Comment se connecter à la devbox ?

> Pour le moment, le script d'installation ne crée pas d'icone.
>
> Pour vous connecter à votre devbox, tapez `wsl -d devbox` dans une invite de commande ou
> dans un terminal.
>
> Lors de la première connexion, la devbox est démarrée et les services comme apache ou mysql sont
> démarrés, mais c'est très rapide.
>
> Vous obtenez alors un prompt sous `bash` et vous pouvez lancer des commandes qui s'exécutent dans
> votre devbox (`ls`, `cd`, `htop`, etc.)
>
> Tapez `exit` pour vous déconnecter de votre devbox (elle continue à fonctionner en tâche de fond).
>
> Si vous utilisez VSCode, celui-ci démarrera automatiquement votre devbox lorsque vous ouvrirez un
> projet ou un workspace stocké dans la devbox.

### Comment voir ce que ça consomme sur mon poste ?

> Toutes les instances WSL (l'infrastructure sur laquelle repose la devbox) sont gérées par le
> processus Windows `Wmmem`.
>
> Vous pouvez ouvrir le gestionnaire de tâches pour voir en temps réel la mémoire RAM et la quantité
> de CPU utilisées par l'ensemble de vos machines virtuelles (y compris docker, etc.)
>
> Le disque virtuel de votre devbox utilise environ 3 Go d'espace disque après l'installation.
> Il augmente ensuite en fonction des projets que vous créez.

### Comment arrêter la devbox ?

> Ouvrez une ligne de commandes et tapez : `wsl --terminate devbox`
>
> Remarque : la devbox est tuée immédiatement, sans avertissement préalable. Assurez-vous que vous
> n'avez pas lancé une tâche de fond importante dans votre devbox.
>
> Seule votre instance devbox est tuée, les autres machines virtuelles éventuelles de votre PC
> continuent à s'exécuter. Pour fermer toutes les machines virtuelles (là encore, sans
>  avertissement préalable), tuez le processus `Wmmem` ou lancez la commande : `wsl --shutdown`

### Comment voir si la devbox est démarrée ?

> Ouvrez une ligne de commandes et tapez : `wsl --list --verbose`
>
> La commande liste toutes les distributions WSL en cours et affiche leur statut.

### Où se trouvent mes fichiers ?

> Le disque virtuel de votre devbox se trouve dans `c:\devbox\distros\devbox\ext4.vhdx`.
>
> C'est ce fichier qu'il faut sauvegarder si vous voulez faire une copie complète de votre devbox
> (arrêtez votre devbox avant de faire la sauvegarde).
>
> Lorsque la devbox est démarrée, ce disque virtuel est accessible sous Windows comme s'il
> s'agissait d'un partage réseau (masqué par défaut). Vous pouvez y accéder en allant sur le
> partage `\\wsl$\devbox` dans l'explorateur Windows.
>
> Vos fichiers se trouvent dans le répertoire `/home/dev` de la devbox, vous pouvez y accéder
> directement en allant dans `\\wsl$\devbox\home\dev` dans l'explorateur Windows.

### Comment accéder à mes sites ?

> Vos sites sont accessibles directement depuis votre navigateur Windows mais sur des ports TCP
> spécifiques :
>
> - Port 33080 pour un accès en http (i.e. port standard 80 + 33000)
> - Port 33443 pour un accès en https (i.e. port standard 443 + 33000)
>
> Par exemple, en tapant `http://localhost:33080` dans votre navigateur, vous accédez en http au
> site apache par défaut de votre devbox.
>
> Pour donner un nom à vos sites web, vous devez les déclarer dans votre fichier etc/host, créer
> un certificat SSL avec mkcert et créer la configuration apache correspondante. (todo: faire doc)

### Comment accéder à MySql ?

> Depuis votre devbox, vous pouvez accéder au prompt mysql en tapant simplement `mysql`.
>
> Vous serez connecté avec le compte utilisateur MySql `dev` qui a tous les droits et aucun mot
>  de passe ne vous sera demandé.
>
> Vous pouvez alors exécuter toutes les commandes et les requêtes SQL que vous voulez :
> - `show databases;`
> - `use mabase;`
> - `select * from wp_options;`
> - `source path.sql` (pour importer un fichier sql)
> - etc.
>
> Tapez `exit` pour sortir du prompt MySql.
>
> Vous pouvez également accéder à MySql depuis Windows en installant l'outil de votre choix.
> Il existe une pléthore d'outils sophistiqués
> ([MySql Workbench](https://www.mysql.com/fr/products/workbench/) par exemple ou bien des
> [extensions pour VScode](https://www.sqlshack.com/visual-studio-code-for-mysql-and-mariadb-development/))
> mais nous recommandons [HeidiSQL](https://www.heidisql.com/) qui est open source,
> léger et performant (il existe même une version portable qu'il suffit de de-zipper).
>
> Une fois que vous avez installé l'outil de votre choix, paramétrer une connexion en indiquant
> les paramètres suivants : `host:127.0.0.1 (ou localhost), port:13306, user:dev, pass:dev`.

### Un éditeur à conseiller ?

> VSCode est gratuit, multi-plateformes, et c'est l'un des meilleurs outils de développement actuel.
>
> Vous pouvez l'installer facilement depuis le site de Microsoft :
>
> - https://code.visualstudio.com/
>
> Une fois que c'est fait, l'extension VScode 'Remote - WSL' vous permet de travailler avec votre
> devbox comme s'il s'agissait de fichiers natifs. Pour installer cette extension tapez `F1`
> dans VScode (command palette) puis `remote-wsl.newWindowForDistro`.

### Un terminal à conseiller ?

> VScode intègre déjà un terminal, mais si vous travaillez souvent en ligne de commandes
> dans votre devbox et que vous voulez un outil plus sophistiqué, cela vaut la peine d'installer
> un terminal un peu plus agréable. Nous vous conseillons d'essayer Windows Terminal :
>
> - https://www.microsoft.com/fr-fr/p/windows-terminal/9n0dx20hk701
>
> Votre devbox sera reconnue automatiquement.
