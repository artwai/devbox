# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# copy-certs.sh

# Ce que fait ce script :
# -----------------------
# Après redémarrage de la devbox, on déplace les CA roots de Windows placé à la racine du
# dossier utilisateur dev (via le script install-devbox.ps1) vers leur place dans
# /root/.local/share/mkcert

mv /home/dev/rootCA.pem /root/.local/share/mkcert/rootCA.pem
mv /home/dev/rootCA-key.pem /root/.local/share/mkcert/rootCA-key.pem