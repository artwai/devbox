OutputSection     "Installation de l'autorité local SSL côté Windows"
OutputColor -Text "Maintenant, on va installer ", "mkcert", " qui va permettre de" -Color Gray, White, Gray
Output            "créer un certificat d'autorité locale afin d'avoir un https sans erreur."
Output
OutputColor -Text "Pour ce faire, on va préalablement installer ", "Scoop", " qui est un" -Color Gray, White, Gray
OutputColor -Text "gestionnaire d'application pour Windows, mais aussi " , "Git", "," -Color Gray, White, Gray
OutputColor -Text "qui est nécessaire pour récupérer ", "mkcert", "." -Color Gray, White, Gray
Output
Output            "Une fenêtre de demande d'installation de certificats va apparaitre lors"
Output            "de la génération des certificats. Il faut les acepter évidemment."
Output

# installation de Scoop. Si déjà présent on le met à jour
if (Test-Path $env:USERPROFILE\scoop\apps)
{
	Output "Scoop est déjà installé, on le met à jour."
	scoop update
}
else
{
    Output "installation de Scoop."
	Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
}
Output

# installation de Git, version sans Git GUI et Git Bash. Si déjà présent on le met à jour
if (Test-Path $env:USERPROFILE\scoop\apps\mingit)
{
    Output "Git est déjà installé, on le met à jour."
	scoop update mingit
}
else
{
    Output "installation de Git."
	scoop install mingit
}
Output

# Ajout des extras dans lesquels se trouve mkcert (nécessite Git)
if (Test-Path $env:USERPROFILE\scoop\buckets\extras)
{
    Output "Les extra buckets de mkcert sont déjà installés, on les supprime pour les réinstaller (mise à jour non disponible)."
	scoop bucket rm extras
}
Output "installation des extra buckets."
scoop bucket add extras

Output

# installation de mkcert. Si déjà présent on le met à jour
if (Test-Path $env:USERPROFILE\scoop\apps\mkcert)
{
    Output "mkcert est déjà installé, on le met à jour."
	scoop update mkcert
}
else
{
    Output "installation de mkcert."
	scoop install mkcert
}
Output

# Génération du certificat d'autorité locale
Output "Génération du certificat d'autorité locale."
mkcert --install