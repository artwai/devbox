# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# modify-hosts.ps1

# Ce que fait ce script :
# -----------------------
# Il récupère le contenu du fichier hosts de Windows et remplace toutes les occurrences d'adresse IP, commençant par "172.", par l'IP transmise en argument.

# Problématique :
# ---------------
# Ce fichier doit être lancé en administrateur et par un autre script Powershell
# C'est seulement possible si les droits d'exécution de fichiers sont changés pour "Unrestricted" ce qui est un risque et pour changer les droits, il faut être déjà en admin...
# Ou alors il faut un script signé, mais ce n'est pas gratuit.
# Tuto pour changer ces droits : http://www.guillaume-p.net/erreur-execution-de-script-est-desactivee-sur-ce-systeme-powershell/

Param
(
    [string] $ip
)

$hostfile = Get-Content "C:\windows\system32\drivers\etc\hosts" -Encoding UTF8 -Raw
$replaced = $hostfile -replace "(172\.[\d\.]+)([\s]+)(.*)","$ip`$2`$3"
Write-Host "Lancement du remplacement des anciennes IP de WSL par $ip"
$replaced | Set-Content -Path "c:\windows\system32\drivers\etc\hosts"

Pause "Tape Entrée pour terminer ce script"