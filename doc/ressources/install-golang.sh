# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# install-go.sh

# Ce que fait ce script :
# -----------------------
# Il installe le langage Go qui permettra de compiler mkcert

# Téléchargement de Golang
echo "Installation Golang"
apt-get -qq install golang > /dev/null

# Affichage de version
go version