﻿<#
 # Autorisation d'excécution
 #>
$exectionPolicy = Get-ExecutionPolicy -Scope LocalMachine

if (($exectionPolicy -notlike "Unrestricted") -and ($exectionPolicy -notlike "Bypass"))
{
	OutputSection     "Impossible d'aller plus loin."
	OutputColor -Text "Ton ordinateur est trop protégé ", "(ExecutionPolicy : $exectionPolicy)", " pour exécuter ce fichier." -Color Gray, Yellow, Gray
	Output            "Tu dois faire des modifications avant de pouvoir exécuter ce script."
	Output            "Dans un terminal Powershell lancé en tant qu'administrateur, lance la commande suivante :",
	OutputColor -Text "Set-ExecutionPolicy Unresctricted" -Color Yellow
	Output
	OutputColor -Text "Pour lancer un terminal Powershell en administrateur, il te suffit de taper ", "Powershell" -Color Gray, White
	Output            "dans la barre de recherche dnas la barre des tâches de Windows."
	OutputColor -Text "Tu devrais voir apparaitre le programme avec l'option ", "Exécuter en tant qu'administrateur",  "." -Color Gray, White, Gray
	Output
	Pause "Tape Entrée pour terminer ce script."
	exit
}
