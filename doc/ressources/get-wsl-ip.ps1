# ----------------------------------------------------------------------------------------------------
# This file is part of DevBox.
#
# Copyright (C) artwai <contact@artwai.com>
#
# For copyright and license information, please view the
# LICENSE file that was distributed with this source code.
# ----------------------------------------------------------------------------------------------------

# get-wsl-ip.ps1

# Ce que fait ce script :
# -----------------------
# Il interroge WSL pour récupérer son IP et lancer un script en admin afin de modifier le fichier hosts

# Problématique :
# ---------------
# Ce fichier doit être lancé en utilisateur courant et non en admin
# Il doit lancer un script en admin mais c'est seulement possible si les droits d'exécution de fichiers l'autorise
# voir modify-hosts.ps1

# récupère le répertoire courant
$runningDirectory = Split-Path -Parent -Path $MyInvocation.MyCommand.Definition

# Interroge WSL pour avoir son IP
$ifconfig = (wsl -- ip -4 addr show eth0)
$ipPattern = "((\d+\.?){4})"
$ip = ([regex]"inet $ipPattern").Match($ifconfig).Groups[1].Value
if (-not $ip) {
    exit
}
Write-Host "L'IP de la devbox est :"
Write-Host $ip

# Lance en admin
Start-Process -FilePath Powershell -Verb RunAs  -ArgumentList '-File', "$runningDirectory\modify-hosts.ps1", '-ip', $ip
