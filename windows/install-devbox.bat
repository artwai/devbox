:: ----------------------------------------------------------------------------------------------------
:: This file is part of DevBox.
::
:: Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
::
:: For copyright and license information, please view the
:: LICENSE file that was distributed with this source code.
:: ----------------------------------------------------------------------------------------------------

:: Ce fichier de commandes permet de télécharger et d'exécuter le script d'installation devbox
:: dans un PowerShell lancé en tant qu'administrateur et en ignorant la  politique d'exécution
:: des scriptsde PowerShell.

:: Principe :
:: ----------
:: - PowerShell.exe dispose d'un paramètre en ligne de commande "-ExecutionPolicy"
::   qui permet de désactiver la politique d'exécution des scripts (bloqué par défaut).
:: - Dans PowerShell, l'applet Start-Process permet de lancer un exécutable en
::   mode administrateur en utilisant le verbe "RunAs".
::
:: On combine ces deux options pour lancer l'installation de la devbox :
:: 1. On télécharge le script d'installation powershell depuis gitlab.
:: 2. On lance un PowerShell standard et on lui demande (via Start-Process)
::    de lancer un second PowerShell en mode admin
:: 3. On demande à ce PowerShell admin d'exécuter notre script en ignorant
::    la politique d'exécution des scripts.

@echo off
setlocal

:: ----------------------------------------------------------------------------------------------------
:: Télécharge le script powershell depuis GitLab
:: ----------------------------------------------------------------------------------------------------
set scriptName=install-devbox.ps1
set scriptUrl=https://gitlab.com/artwai/devbox/-/raw/master/windows/%scriptName%
set scriptPath=%TEMP%\%scriptName%
curl -s -o %scriptPath% %scriptUrl%

:: ----------------------------------------------------------------------------------------------------
:: Lance le script en mode admin et sans ExecutionPolicy
:: ----------------------------------------------------------------------------------------------------
set args=-NoExit -NoLogo -NoProfile -ExecutionPolicy Bypass -File ""%scriptPath%""
set StartProcess=Start-Process -Verb RunAs -WindowStyle maximized PowerShell.exe '%args%'
PowerShell.exe -NoExit -command "%StartProcess%"

:: commande complète générée :
:: PowerShell.exe -command "Start-Process -Verb RunAs -WindowStyle maximized PowerShell.exe '-NoLogo  -NoProfile -ExecutionPolicy Bypass -File ""C:\Users\toto\AppData\Local\Temp\install-devbox.ps1""'"
