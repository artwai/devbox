﻿<# ----------------------------------------------------------------------------------------------------
 # This file is part of DevBox.
 #
 # Copyright (C) Daniel Ménard <daniel.menard.35@gmail.com>
 #
 # For copyright and license information, please view the
 # LICENSE file that was distributed with this source code.
 # ---------------------------------------------------------------------------------------------------- #>

 # WARNING : CE FICHIER EST EN UTF-8-BOM, VERIFIEZ QUE VOTRE EDITEUR L'A CHARGE CORRECTEMENT

<# ----------------------------------------------------------------------------------------------------
 # Paramètres de notre script
 # ---------------------------------------------------------------------------------------------------- #>
# Répertoire de base des fichiers générés par le script d'installation
$devboxBaseDirectory = "C:\devbox"

# Répertoire des fichiers temporaires
$devboxTempDirectory = "$devboxBaseDirectory\temp"

# Répertoire des distros
$devboxDistroDirectory = "$devboxBaseDirectory\distros" # contient un sous répertoire par devbox

# Répertoire des certificats d'autorité racine générés par mkcert
$devboxCarootDirectory = "$devboxBaseDirectory\caroot"

# Nom par défaut de la devbox. L'utilisateur peut choisir un autre nom pour créer plusieurs devbox
$devboxDefaultName = "devbox2"

# Url de base des scripts exécutés dans WSL
$baseUrlForWslScripts = 'https://gitlab.com/artwai/devbox/-/raw/master/ubuntu/'

# Nom de la clé du registre qui contient les paramètres WSL (Lxss)
$wslRegistry = "Registry::HKCU\Software\Microsoft\Windows\CurrentVersion\Lxss"

# Paramètres du script (nom, version, etc.) affichés lorsque le script est lancé
$script = [PSCustomObject]@{
    Name = 'install-devbox'
    Version = '0.6.0'
    Description = 'Installe une distro WSL qui contient les outils dont tu as besoin'
    Welcome = @(
        "Salut à toi Art'ïsan du web !"
        ""
        "Ce script va installer une « devbox » sur ton PC : une machine virtuelle (sous WSL) avec des"
        "outils comme apache, php, mysql et d'autres."
        ""
        "L'installation prends un peu de temps (il faut télécharger les logiciels), donc soit patient(e),"
        "prends-toi une boisson chaude (ou un apéro selon l'heure) et installes-toi confortablement !"
        ""
        "Ah encore une chose : il faudra peut-être redémarrer ton PC et on ne peut pas exclure que ce"
        "script plante lamentablement, donc avant de continuer, assures-toi que tu as bien enregistré"
        "tous tes documents en cours et fermé les applications dont tu n'as pas besoin."
    )
}


# Liste des scripts à exécuter dans WSL (Installation du serveur)
# Pour chaque section, on a :
# - script (obligatoire) : nom du script à télécharger et à exécuter (sur gitlab/devbox)
# - section (optionnel, nom du script par défaut) : titre à afficher pour cette étape
# - before (optionnel) : ligne(s) de texte à afficher avant de lancer le script
# - after (optionnel) : ligne(s) de texte à afficher après l'exécution du script
$wslScriptsInstall = @(
    @{
        'script'="upgrade-linux.sh"
        'section'="Première chose à faire : mise à jour de Linux"
        'before' = @(
            "- Je vais lancer les utilitaires de mise à jour et d'upgrade de la distro Linux installée"
        )
    }
    @{
        'script'="create-user.sh"
        'section'="Création de ton compte utilisateur"
        'before' = @(
            "- Je vais créer le compte utilisateur de ta devbox (ton compte linux)."
            "  Ce compte n'a rien à voir avec ton compte Windows, c'est que pour ta devbox."
            "- Je vais tout paramétrer pour que tu n'aies jamais besoin de les saisir"
            "  mais au cas où, le login c'est « dev » et le mot de passe c'est « dev »"
        )
    }
    @{
        'script'="generate-sshkey.sh"
        'section'="Génération de ta clé SSH"
        'before' = @(
            "- La clé SSH permet d'automatiser la communication avec Git et ne pas utiliser"
            "  ton login et mot de passe à partir de ce PC."
            "- Je vais la générer et te l'afficher afin que tu la places dans ton compte"
            "  sur Gitlab https://gitlab.com/-/profile/keys."
        )
    }
    @{
        'script'="install-git.sh"
        'section'="Installation de Git"
        'before' = @(
            "- Git est un outil de versionning de code qui permet de travailler avec des forges"
            "  logicielles comme GitHub, GitLab ou Bitbucket."
            "- Je vais installer Git et le paramétrer pour que tu puisses travailler."
        )
    }
    @{
        'script'="install-apache.sh"
        'section'="Installation de Apache HTTPD Server"
        'before' = @(
            "- Je vais installer Apache et le configurer pour qu'il écoute sur les ports"
            "  80 (http) et 443 (https)"
        )
    }
    @{
        'script'="install-php.sh"

        'section'="Installation de PHP"
        'before' = @(
            "- Je vais installer plusieurs versions de PHP et les extensions habituelles"
        )
    }
    @{
        'script'="install-mysql.sh"
        'section'="Installation de MySQL 8"
        'before' = @(
            "- Je vais installer MySQL 8, la dernière version de MySql"
        )
    }
    @{
        'script'="install-wp-cli.sh"
        'section'="Installation de WP Cli"
        'before' = @(
            "- Je vais installer WP Cli permettant d'effectuer, en ligne de commande, toute action"
            "  qu'il serait utile de faire dans l'administration WordPress ou la base de données."
        )
    }
    @{
        'script'="install-nodejs.sh"
        'section'="Installation de NodeJS"
        'before' = @(
            "- Je vais installer NodeJS permettant d'utiliser Gulp."
        )
    }
    @{
        'script'="install-python2.sh"
        'section'="Installation de Python 2"
        'before' = @(
            "- Je vais installer Python 2."
        )
    }
    @{
        'script'="install-composer.sh"
        'section'="Installation de Composer"
        'before' = @(
            "- Je vais installer Composer 1 et 2."
        )
    }
    @{
        'script'="install-unzip.sh"
        'section'="Installation de unzip"
        'before' = @(
            "- Je vais installer unzip."
        )
    }
    @{
        'script'="install-jq.sh"
        'section'="Installation de jq"
        'before' = @(
            "- Je vais installer jq."
        )
    }
    @{
        'script'="install-mailhog.sh"
        'section'="Installation de MailHog"
        'before' = @(
            "- Je vais installer MailHog."
        )
    }
    @{
        'script'="install-mkcert.sh"
        'section'="Installation de l'autorité local SSL côté devbox"
        'before' = @(
            "- Même principe que pour Windows, je vais installer mkcert."
        )
    }
)

# Liste des scripts à exécuter dans WSL (Post installation du serveur)
# Pour chaque section, on a :
# - script (obligatoire) : nom du script à télécharger et à exécuter (sur gitlab/devbox)
# - section (optionnel, nom du script par défaut) : titre à afficher pour cette étape
# - before (optionnel) : ligne(s) de texte à afficher avant de lancer le script
# - after (optionnel) : ligne(s) de texte à afficher après l'exécution du script
$wslScriptsPostInstall = @(
    @{
        'script'="restart-wsl.sh"
        'section'="no-section"
        'before' = @(
            "- On redémarre"
        )
    }
    @{
        'script'="create-dbuser.sh"
        'section'="Création d'un utilisateur MySQL"
        'before' = @(
            "- On va créer un utilisateur pour accéder facilement aux bases de données"
            "  le login c'est « dev » et le mot de passe c'est « dev »"
        )
    }
)


<# ----------------------------------------------------------------------------------------------------
 # Fonctions
 # ---------------------------------------------------------------------------------------------------- #>

# Source pour obtenir des infos sur la version de Windows :
# https://smsagent.blog/2017/05/18/find-the-full-windows-build-number-with-powershell/

<#
 # Retourne le nom de la version de Windows
 #>
function GET-WindowsProductName {
    return (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' –Name ProductName).ProductName
}

<#
 # Retourne le numéro de version (Build Number) Windows
 #>
function Get-WindowsBuildNumber {
    return (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' –Name CurrentBuild).CurrentBuild
}

<#
 # Teste si WSL ets installé
 #>
function hasWSL {
    return (Get-WindowsOptionalFeature -FeatureName Microsoft-Windows-Subsystem-Linux -Online).State -eq "Enabled"
}

<#
 # Retourne le numéro de version (Build Number) Windows
 #>
function SysInfo {
    $productName = GET-WindowsProductName
    $build = GET-WindowsBuildNumber
    $wsl = if (hasWSL) {"WSL activé"} else {"WSL non disponible"}
    $free = [Math]::Round((get-psdrive c).free / 1Gb, 2);

    Write-Line "Info système : $productName, build $build, $wsl, $free Go dispo sur C"  "White"
}

<#
 # Crée la struture du répertoire de base (c:\devbox)
 #
 #>
function CreateBaseDirectoryStructure() {
    New-Item -ItemType Directory -Force -Path $devboxBaseDirectory | Out-Null
    New-Item -ItemType Directory -Force -Path $devboxTempDirectory | Out-Null
    New-Item -ItemType Directory -Force -Path $devboxDistroDirectory | Out-Null
    New-Item -ItemType Directory -Force -Path $devboxCarootDirectory | Out-Null
}

<#
 # Affiche une ligne de texte, éventuellement colorée.
 #>
function Write-Line
{
    param (
        [Parameter(Mandatory)]
        [Object]
        # Texte à afficher
        $text,
        [ConsoleColor]
        # Couleur à utiliser
        $color = [ConsoleColor]::Gray
    )

    Write-Host $text -ForegroundColor $color
}

<#
 # Affiche plusieurs bouts de texte avec une couleur différente pour chaque bout.
 #
 # Exemple :
 # Write-LinesColors -Text "Red ", "Green ", "Yellow " -Color Red,Green,Yellow
 #
 # source : https://stackoverflow.com/a/36519870
 #>
function Write-LinesColors
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [Object[]]
        # Tableau de textes à afficher
        $text,
        [ConsoleColor[]]
        # Tableau de couleurs à utiliser
        $color = @([ConsoleColor]::Gray)
    )

    $DefaultColor = $Color[0]

    if ($color.Count -ge $text.Count) {
        for ($i = 0; $i -lt $text.Length; $i++) {
            Write-Host $text[$i] -ForegroundColor $color[$i] -NoNewLine
        }
    } else {
        for ($i = 0; $i -lt $color.Length ; $i++) {
            Write-Host $text[$i] -ForegroundColor $color[$i] -NoNewLine
        }
        for ($i = $color.Length; $i -lt $text.Length; $i++) {
            Write-Host $text[$i] -ForegroundColor $defaultColor -NoNewLine
        }
    }
    Write-Host
}

<#
 # Affiche un titre.
 #>
function Write-Title
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Titre à afficher
        $text
    )

    Write-Host
    Write-Line $text ([ConsoleColor]::Yellow)
    Write-Line $("=" * $text.length) ([ConsoleColor]::Yellow)
}

<#
 # Affiche un sous-titre.
 #>
function Write-Section
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Sous-titre à afficher (en jaune)
        $text
    )

    Write-Host
    Write-Line $text ([ConsoleColor]::Yellow)
    Write-Line $("-" * $text.length) ([ConsoleColor]::Yellow)
}

<#
 # Affiche un message de réussite.
 #>
function Write-Success
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Texte de succès à afficher (en vert)
        $text
    )

    Write-Host
    Write-Line $text ([ConsoleColor]::Green)
}

<#
 # Affiche un message de warning.
 #>
function Write-Warning
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Texte de warning à afficher (en jaune)
        $text
    )

    Write-Host
    Write-Line $text ([ConsoleColor]::Yellow)
}

<#
 # Affiche une erreur
 #>
function Write-LineError
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Texte d"erreur à afficher (en rouge)
        $text
    )

    Write-Line $text ([System.ConsoleColor]::Red)
}

<#
 # Demande à l'utilisateur de saisir quelque chose ou de valider la valeur par défaut
 # et retourne le résultat.
 #>
function Read-Input
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Message à afficher
        $message,
        [string]
        # Valeur de défaut à afficher
        $default = ''
    )

    if ($default -eq "") {
        Write-Host $message -ForegroundColor ([ConsoleColor]::White) -NoNewLine
    } else {
        Write-Host $message -ForegroundColor ([ConsoleColor]::White) -NoNewLine
        Write-Host " [" -ForegroundColor ([ConsoleColor]::White) -NoNewLine
        Write-Host $default -ForegroundColor ([ConsoleColor]::Cyan) -NoNewLine
        Write-Host "] > " -ForegroundColor ([ConsoleColor]::White) -NoNewLine
    }

    $value = Read-Host
    if ($value -eq "") {
        return $default
    }
    return $value
}

<#
 # Demande à l'utilisateur de taper Entrée pour continuer.
 #>
function Write-Pause
{
    param (
        [string]
        # Message d'action à afficher
        $message = "Tape Entrée pour continuer"
    )

    Write-Host
    Write-Host $message -ForegroundColor ([ConsoleColor]::Cyan) -NoNewLine
    Write-Host "... " -ForegroundColor ([ConsoleColor]::Cyan) -NoNewLine
    Read-Host > $null
}

<#
 # Retourne l'UUID de la distro WSL par défaut (chaine vide si aucune)
 #>
function Get-DistroDefaultUuid
{
    try {
        $key = Get-ItemProperty -ErrorAction Stop -Path $wslRegistry -Name DefaultDistribution

        return $key.DefaultDistribution
    } catch {
        return ""
    }
}

<#
 # Retourne le nom de la distro ayant l'UUID indiqué (chaine vide si UUID non trouvé)
 #>
function Get-DistroName
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # uuid à tester
        $uuid
    )

    try {
        $path = (Join-Path $wslRegistry $uuid)
        $key = Get-ItemProperty -ErrorAction Stop -Path $path -Name DistributionName
        return $key.DistributionName
    } catch {
        return ""
    }

    return "";
}

<#
 # Retourne le nom des distro existantes (tableau vide si aucune)
 #>
function Get-ExistingDistroNames
{
    try {
        $uuids = Get-ChildItem -ErrorAction Stop -Path $wslRegistry -Name
    } catch {
        return @()
    }

    $names = @()
    for ($i = 0; $i -lt $uuids.Length; $i++) {
        $names += Get-DistroName $uuids[$i]
    }

    return $names;
}

<#
 # Teste s'il existe déjà une distro avec le nom indiqué (insensible à la casse).
 #>
function Test-DistroExists
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Nom de la distro à tester
        $name
    )

    $names = Get-ExistingDistroNames

    return $name -in $names
}

<#
 # Exécute un script dans la distro.
 #
 # La méthode se connecte à la distro (avec le user "root"), télécharge le script indiqué depuis
 # le gitlab devbox et l'exécute.
 #
 # @param string $scriptName : nom du script à exécuter (par exemple "setup-user.sh")
 #>
function Invoke-WslRunScript
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string]
        # Nom du script à lancer
        $scriptName
    )

    $scriptUrl = $baseUrlForWslScripts + $scriptName

    $command = 'wsl -d {0} -u root -- bash -l -c "wget {1} -q -O - | bash"' -f $devboxName, $scriptUrl

    try {
        Invoke-Expression $Command -ErrorAction Stop
    } catch {
        Write-LineError $_.Exception.Message
    }

    #return $LastExitCode;
}

<#
 # Exécute tous les scripts définis dans la variable passée.
 #>
function WSL-Install($wslScripts)
{
    for ($i=0 ; $i -lt $wslScripts.Length ; $i++) {
        $script = $wslScripts[$i]

        if ($script.ContainsKey('section')) {
            if ($script.section -ne "no-section") {
                Write-Section ($script.section + " :")
            }
        } else {
            Write-Section ($script.script + " :")
        }

        if ($script.ContainsKey('before')) {
            Write-Line ($script.before | Out-String).Trim()
        }
        Write-Host

        Invoke-WslRunScript $script.script

        if ($script.ContainsKey('after')) {
            Write-Host
            Write-Line ($script.after | Out-String).Trim()
        }

        Write-Pause
    }
}


<# ----------------------------------------------------------------------------------------------------
 # START
 # ---------------------------------------------------------------------------------------------------- #>

Clear-Host
$Host.UI.RawUI.ForegroundColor = "Gray"
$Host.UI.RawUI.WindowTitle = "$($script.Name) version $($script.Version)"

Write-Title "$($script.Name) : $($script.Description) (version $($script.Version))"
Write-Line ($script.Welcome | Out-String)
SysInfo
Pause "Tape Entrée pour continuer ou Ctrl+C pour interrompre ce script" "ENTREE"

<#
 # Vérifie la version de Windows, exit si version trop ancienne.
 #>
$currentBuildNumber = Get-WindowsBuildNumber
$minimumBuildNumber = 19043
if ($currentBuildNumber -lt $minimumBuildNumber) {
    Write-Host
    Write-LineError "Ah zut ! pour exécuter ce script, il faut la version $minimumBuildNumber ou plus de Windows,"
    Write-LineError "mais toi tu as la version $currentBuildNumber, donc je ne peux pas continuer."
    Write-LineError "Lance Windows Update pour mettre à jour ta version de Windows et relance ce script ensuite."
    Write-Pause "Tape Entrée pour terminer ce script"
    exit 1
}

<#
 # Teste si WSL est installé, propose l'installation si ce n'est pas le cas
 #>
if (! (hasWSL) ) {
    Write-Section "Installation de WSL"
    Write-Line "Pour utiliser ce script, des composants Windows optionnels de WSL doivent être activés."
    Write-Host

    choice /m "Veux-tu que je le fasse (un redémarrage sera peut-être nécessaire) ?"
    if ($LASTEXITCODE -eq 1) {
        Write-Host
        Write-Line "Activation du composant Microsoft-Windows-Subsystem-Linux"
        Write-Line "---------------------------------------------------------"
        dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

        Write-Host
        Write-Line "Activation du composant VirtualMachinePlatform"
        Write-Line "----------------------------------------------"
        dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart

        Write-Host
        Write-Line "Les composants Windows ont été activés"
        Write-Line "--------------------------------------"
        Write-Line "Vérifie que les commandes ci-dessus n'ont pas généré d'erreurs puis"
        Write-Line "télécharges et installes le fichier https://aka.ms/wsl2kernel."
        Write-Host
        Write-LinesColors "Dans tous les cas, il faudra ensuite redémarrer ton ordinateur" "Yellow"
        Write-Line "(l'installation ne fonctionnera pas si tu ne redémarres pas ton PC, les commandes"
        Write-Line "exécutées demandent juste à Windows d'installer WSL au prochain démarrage)."
        Write-Host
        Write-Success "Une fois que tu as installé WSL2, relances ce script pour installer ta devbox."
    } else {
        Write-LineError "La devbox ne peut pas être installée."
    }

    Pause
    exit 1
}


<#
 # Choix du nom de la devbox
 #>
Write-Section "Nom de ta devbox :"
Write-Line "Comment veux-tu appeler ta devbox ?"
while ($true) {
    Write-Host
    $devboxName = Read-Input "Nom de la machine virtuelle" $devboxDefaultName

    if (Test-DistroExists $devboxName) {
        Write-Host
        Write-LineError "T'as déjà une distro qui s'appelle comme ça !"
        Write-LineError "Choisis un autre nom (ou supprime la distro existante et ré-essaye)"
        continue
    }

    if ($devboxName -notmatch "^[A-Za-z0-9\._-]{1,15}$") {
        Write-Host
        Write-LineError "Ce n'est pas terrible comme nom de distro..."
        Write-LineError "Essaie de trouver un nom court (max 15) avec juste des lettres, des chiffres et les caractères '.-_'"
        continue
    }
    break
}

<#
 # Crée l'arborescence du répertoire de base (c:\devbox)
 #>
CreateBaseDirectoryStructure

<#
 # Répertoire des fichiers temporaires
 #>
<#
# pas très utile
Write-Section "Pour info :"
Write-Line "- Les fichiers temporaires générés par ce script seront stockés dans le répertoire :"
Write-Line "  $($devboxTempDirectory)" "White"
Write-Line "- Ce répertoire ne sera pas effacé à la fin de l'installation car si tu relances ce script"
Write-Line "  ça m'évitera de télécharger à nouveau les fichiers dont j'ai besoin, mais si un jour tu"
Write-Line "  es à court de place sur ton disque, tu pourras sans problème le supprimer."
Pause
#>

<#
 # Téléchargement de l'image rootfs
 #>
Write-Section "Récupération de l'image Ubuntu 24.04 :"
$rootfsPath = "$($devboxTempDirectory)\rootfs-ubuntu-24.04.tar.gz";
if(Test-Path $rootfsPath) {
    Write-LinesColors "Utilisation de l'image existante : ", $rootfsPath "Gray", "White"
} else {
    $rootfsUrl = "https://cloud-images.ubuntu.com/wsl/noble/current/ubuntu-noble-wsl-amd64-ubuntu24.04lts.rootfs.tar.gz";

    Write-Line "Je vais télécharger l'image depuis le site officiel :"
    Write-LinesColors "- de : ", $rootfsUrl "Gray", "White"
    Write-LinesColors "- vers : ", $rootfsPath "Gray", "White"
    Write-Host
    Write-Line "Cela représente environ 350 Mo à télécharger, donc ça va prendre un peu de temps"
    Write-Line "en fonction de ta connexion internet."

    Start-BitsTransfer -Source $rootfsUrl -Destination $rootfsPath -DisplayName "Téléchargement de Ubuntu 24.04"

    Write-Line "Bon c'était long, mais on est arrivés à charger le morceau !"
    Pause
}

<#
 # Choix du répertoire d'installation
 #>
Write-Section "Choix du répertoire d'installation de ta devbox :"
$defaultDevboxPath = (Join-Path $devboxDistroDirectory $devboxName)

Write-LinesColors "Par défaut, ta devbox sera installée dans le répertoire : ", $defaultDevboxPath "Gray", "White"
Write-Line "Ce répertoire contiendra le disque virtuel de ta devbox (un fichier "
Write-LinesColors "ext4.vhdx", " qui contiendra tous tes fichiers)." "White", "Gray"
Write-Line "Si tu n'as pas trop de place sur le disque C, tu peux choisir un autre répertoire."
Write-Line "Choisis de préférence un disque SSD sinon les performances seront un peu moins bonnes."

while ($true) {
    Write-Host
    $devboxPath = Read-Input "Répertoire de ta devbox" $defaultDevboxPath

    if (Test-Path $devboxPath) {
        $vhdxPath = (Join-Path $devboxPath "ext4.vhdx")
        if (Test-Path $vhdxPath) {
            Write-Host
            Write-LineError "Le répertoire indiqué existe et contient déjà un disque virtuel."
            Write-LineError "Attention, tu risques d'écraser une installation existante."
            Write-LineError "Fais du ménage et ré-essaie ou choisit un autre répertoire de destination."
            continue
        }
        Break # ok, le répertoire devbox existe mais pas le fichier ext4 (ancienne distro désinstallée)
    }

    try {
        New-Item -ItemType Directory -ErrorAction Stop -Path $devboxPath > $null
    } catch {
        Write-LineError $_
        continue
    }

    Break
}

$devboxPath = (Resolve-path $devboxPath).Path

<#
 # Création de la distro WSL
 #>
Write-Section "Création de ta devbox :"
Write-LinesColors "Création de ta devbox dans le répertoire ", $devboxPath "Gray", "White"
Write-Line "Je ne peux pas afficher de barre de progression, il faut juste patienter."

wsl --import $devboxName $devboxPath $rootfsPath --version 2

Write-Line "C'est fait, ta distribution Ubuntu a été créée."
Pause

<#
 # Téléchargement de mkcert et création du certificat d'autorité racine
 #>

# alternative à mkcert : https://gist.github.com/cecilemuller/9492b848eb8fe46d462abeb26656c4f8

Write-Section "Création d'une autorité de certification"
Write-Line "Pour que les sites web que tu vas créer fonctionnent localement en HTTPS, il faut qu'ils"
Write-Line "aient un certificat SSL signé par une autorité de certification reconnue par ton navigateur."
Write-Line "Pour cela, je vais télécharger un outil (mkcert) qui me servira à créer une nouvelle "
Write-Line "autorité de certification (un certificat racine utilisable uniquement sur ton poste)."
Write-Line "Je vais ensuite ajouter ce certificat racine dans le coffre-fort de Windows qui t'affichera"
Write-Line "un popup de confirmation (il faudra répondre « oui »)."
Write-Host

$mkcert = "$devboxBaseDirectory\mkcert.exe"
if (!(Test-Path $mkcert)) {
    Write-LinesColors "- téléchargement de mkcert dans : ", $mkcert "Gray", "White"
    $url = 'https://github.com/FiloSottile/mkcert/releases/download/v1.4.4/mkcert-v1.4.4-windows-amd64.exe'
    Invoke-RestMethod -Uri $url -OutFile $mkcert
} else {
    Write-LinesColors "- mkcert est déjà disponible dans : ", $mkcert "Gray", "White"
}

Write-Line "- création du certificat racine de l'autorité de certification (CAROOT)"

# Ressources mkcert/certificats/etc. :
# https://gist.github.com/cecilemuller/9492b848eb8fe46d462abeb26656c4f8#windows-10-firefox
# https://ddev.readthedocs.io/en/stable/#windows-and-firefox-mkcert-install-additional-instructions
# https://support.umbrella.com/hc/en-us/articles/115000669728-Configuring-Firefox-to-use-the-Windows-Certificate-Store
# https://github.com/FiloSottile/mkcert/issues/239
# https://blog.mozilla.org/security/2020/04/14/expanding-client-certificates-in-firefox-75/
# https://wiki.mozilla.org/CA/AddRootToFirefox


# Indique à mkcert le répertoire où on veut qu'il crée le certificat racine
setx CAROOT $devboxCarootDirectory | Out-Null       # au niveau système
$Env:caroot=$devboxCarootDirectory                  # pour la session en cours

# Crée les certificats racine
# &$mkcert -install 2>&1 | Out-null                   # crée le certificat
&$mkcert -install                   # crée le certificat

$rootCA = "$devboxCarootDirectory\rootCA.pem"
$rootCAkey =  "$devboxCarootDirectory\rootCA-key.pem"
if (!((Test-Path $rootCA) -and (Test-Path $rootCAkey))) {
    Write-LineError "mkcert n'a pas créé les fichiers certificat, impossible de continuer"
    exit
}

Write-LinesColors "- les fichiers ", $rootCA, " et ", $rootCAkey, " ont bien été créés" "Gray", "White", "Gray", "White", "Gray"

# Récupère l'empreinte du fichier rootCA.pem généré par mkcert
$thumbPrint = (Get-PfxCertificate -FilePath 'C:\devbox\caroot\rootCA.pem').Thumbprint

# Vérifie que le certificat a été ajouté dans les certificats racines de Windows
if (!(Test-Path "Cert:\CurrentUser\Root\$thumbPrint")) {
    Write-LineError   "- Le certificat n'a pas été correctement enregistré dans Windows."
    Write-Warning "  Il y a un souci quelconque sur ton poste qui fait que le certificat racine"
    Write-Warning "  ne peut pas être ajouté automatiquement dans le magasin de certificats de"
    Write-Warning "  Windows (stratégie de groupe ? clé de licence en volume ?)"
    Write-Warning "  L'installation de la devbox peut continuer, mais tu devras importer le"
    Write-Warning "  certificat racine manuellement (todo: faire doc pour expliquer comment)."
    pause
} else {
    Write-Line "- Le certificat a été correctement enregistré dans le magasin de certificats Windows,"
}

Write-Line "- Le certificat racine importé dans Windows sera reconnu automatiquement par Chrome et Edge."
Write-Line "- Par contre, par défaut, Firefox n'utilise pas le magasin de certificats Windows :"
Write-Line "  si tu utilises FireFox, tu devras soit mettre à true le paramètre "
Write-Line "  security.enterprise_roots.enabled (cf. https://serverfault.com/a/821200), soit "
Write-Line "  importer manuellement le fichier $rootCA (cf. https://bit.ly/3Hzusv2)."
Write-Host
Pause

# Demande à windows de transmettre la variable d'environnement CAROOT à WSL
If ($Env:WSLENV -notlike "*CAROOT/up*") {
    setx WSLENV "CAROOT/up:$Env:WSLENV"
}


<#
 # Création d'un raccourci
 #
 # source : https://docs.microsoft.com/fr-fr/troubleshoot/windows-client/admin-development/create-desktop-shortcut-with-wsh
 #>
# désactivé, c'est pas au point et ça n'apporte pas grand chose
<#
    $shell = New-Object -ComObject WScript.Shell
    $explorerFile = "C:\Windows\explorer.exe"

    $parent = $shell.SpecialFolders("AllUsersPrograms")
    $linkName = $devboxName + ".lnk"
    $linkPath = (Join-Path $parent $linkName)
    $targetPath = "C:\Windows\System32\wsl.exe"
    $arguments = " -d " + $devboxName
    If (Test-Path $linkPath){
        Remove-Item $linkPath
    }

    $shortcut = $shell.CreateShortcut($linkPath)
    $shortcut.TargetPath = $targetPath
    $shortcut.Arguments = $arguments
    $shortcut.WorkingDirectory = "\\wsl$\" + $devboxName + "\home\dev"
    $shortcut.Description = "Ouvre un shell sur la distribution WSL " + $devboxName
    $shortcut.IconLocation = "explorer.exe,12"
    $shortcut.WindowStyle = 3 # maximized
    $shortcut.Save()
#>

<#
 # Installation - lancement des scripts serveur
 #>
WSL-Install $wslScriptsInstall

<#
 # Redémarrage de la devbox (entres autres pour que wsl.conf prenne effet)
 #>
Write-Section "Redémarage de ta devbox pour lancer les services"
wsl --terminate $devboxName
Write-Line "- Arrêt effectué."

<#
 # Installation - lancement des scripts serveur post redémarrage
 #>
WSL-Install $wslScriptsPostInstall

# Extinction finale de la devbox
wsl --terminate $devboxName

<#
 # Terminé
 #>
Write-Section "Terminé !"
Write-Line "Bon je ne sais pas si tu as terminé ta boisson, mais moi j'ai fini !"
Write-Host

Write-Success "Ta devbox est prête"
Write-Host
Write-Line "Tu peux y accéder :"
Write-LinesColors "- en tapant ", "wsl -d $($devboxName)", " en ligne de commande" "Gray", "Yellow", "Gray"
Write-Line "- en ouvrant une session dans Windows Terminal (si installé)"
Write-LinesColors "- en accédant à ", "\\wsl$\$($devboxName)", " dans l'explorateur Windows" "Gray", "Yellow", "Gray"
Write-LinesColors "- en ouvrant une session `"remote`" sur ", $devboxName, " dans Visual Studio Code" "Gray", "Yellow", "Gray"
Write-Host

Write-Line "Si tu veux supprimer ta devbox :"
Write-Line "- Je serai triste parce que je me suis donné beaucoup de mal pour la créer !"
Write-Line "- Mais tu peux le faire en tapant la commande suivante dans un terminal :"
Write-Line "  wsl --unregister $($devboxName)" "yellow"
Write-Line "- Attention, la commande supprime immédiatement ta devbox, il n'y a aucune confirmation"
Write-Line "  et ça supprime définitivement le disque virtuel de ta devbox et ça la désinscrit."
Write-Line "- Donc vérifie avant que tu n'as vraiment plus rien à récupérer dessus..."
Write-Host

Write-Line "Allez, A+"

Pause "Tape Entrée pour terminer ce script"
